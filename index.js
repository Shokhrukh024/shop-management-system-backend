const express = require('express')
const mongoose = require('mongoose')

const app = express()

// mongoose.connect('mongodb://localhost:27017/my-db', {useNewUrlParser: true, useUnifiedTopology: true})
mongoose.connect('mongodb+srv://Shokhrukh024:*5101923Sh@cluster1.jle1c.mongodb.net/myDb?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Connected to database'))

app.use(express.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

const usersRouter = require('./routes/users')
app.use('/users', usersRouter)

const todosRouter = require('./routes/todos')
app.use('/todos', todosRouter)

// const productsRouter = require('./routes/products')
// app.use('/products', productsRouter)

const customersRouter = require('./routes/customers')
app.use('/customers', customersRouter)


const producersRouter = require('./routes/producers')
app.use('/producers', producersRouter)


app.listen(process.env.PORT || 3000, () => console.log('Server started!'))
