const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
        // default: Date.now
    },
    amount: {
        type: Number,
        required: true,
    },
    buyPrice: {
        type: Number,
        required: true,
    },
    sellPrice: {
        type: Number,
        required: false,
    },
    about: {
        type: String,
        required: false,
    }
})

module.exports = mongoose.model('productCustomerDetail', schema)
