const mongoose = require('mongoose')
// var aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    // date: {
    //     type: String,
    //     required: true,
    // },
    // customer_name: {
    //     type: String,
    //     required: true,
    // },
    user_id: String,
    customer_name: String,
    date: Date,
    barcode: String,
    name: String,
    product_id: String,
    amount: Number,
    measure: String,
    buyPrice: String,
    payed: String
    // about: {
    //     type: String,
    //     required: false,
    // },
})

schema.plugin(mongoosePaginate);
// schema.plugin(aggregatePaginate);


module.exports = mongoose.model('productsDefaultCustomer', schema)
