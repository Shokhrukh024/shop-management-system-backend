const mongoose = require('mongoose')
const Products_customer = require('./product-customer')
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    // _id: new mongoose.Schema.Types.ObjectId(),
    user_id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: Number,
        required: false,
    },
    companyName: {
        type: String,
        required: false,
    },
    productArr: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Products_customer
    }]
})

schema.plugin(mongoosePaginate);

module.exports = mongoose.model('customers', schema)
