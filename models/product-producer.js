const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2');
const Product_producer_detail = require('./product-producer-detail')

const schema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
        select: false,
    },
    barcode: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    totalAmountLeft: {
        type: String,
        required: false,
    },
    measure: {
        type: String,
        required: true,
    },
    // buyPrice: {
    //     type: Number,
    //     required: true,
    // },
    sellPrice: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    productDetailArr: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Product_producer_detail,
    }]
})
schema.plugin(mongoosePaginate);

module.exports = mongoose.model('productProducer', schema)
