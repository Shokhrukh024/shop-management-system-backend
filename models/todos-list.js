const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    todos: [
        {
            title: String,
            task_type: String
        }
    ],
    user_id: {
        type: String,
        required: true,
    },
})

module.exports = mongoose.model('todos', schema)
