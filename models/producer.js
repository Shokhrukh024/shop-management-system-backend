const mongoose = require('mongoose')
const Product_producer = require('./product-producer')
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
    // _id: new mongoose.Schema.Types.ObjectId(),
    user_id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: Number,
        required: false,
    },
    companyName: {
        type: String,
        required: false,
    },
    productArr: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: Product_producer
    }]
})

schema.plugin(mongoosePaginate);

module.exports = mongoose.model('producers', schema)
