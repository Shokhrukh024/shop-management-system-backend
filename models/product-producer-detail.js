const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2');
const schema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
        select: false,
    },
    date: {
        type: Date,
        required: true,
        // default: Date.now
    },
    amount: {
        type: Number,
        required: true,
    },
    amountLeft: {
        type: Number,
        required: true,
    },
    buyPrice: {
        type: String,
        required: true,
    },
    // sellPrice: {
    //     type: String,
    //     required: false,
    // },
    payed: {
        type: String,
        required: false,
    },
    unPayed: {
        type: String,
        required: false,
    },
    about: {
        type: String,
        required: false,
    }
})
schema.plugin(mongoosePaginate);


module.exports = mongoose.model('productProducerDetail', schema)
