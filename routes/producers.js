const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

const Producer = require('../models/producer')
const Product_producer = require('../models/product-producer')
const Product_producer_detail = require('../models/product-producer-detail')


//Getting all producers
router.get('/get/all/:page/:user_id', verifyToken, async (req, res) => {
    try{
        var query = {user_id: req.params.user_id};
        var options = {
            page: req.params.page,
            limit: 10,
            select: '-productArr'
        };

        Producer.paginate(query, options).then(function (result) {
            res.json(result);
        }).catch(function(error) {
            console.log(error); 
        })

        // const producer = await Producer.find();
        // res.json(producer);
    }catch(error){
        res.status(500).json({message: error.message});
    }
})
//Filter by name 
router.get('/filter/by/:name/:user_id', verifyToken, async (req, res) => {
    let product;
    try {
        product = await Product_producer.find(
            {name: { "$regex": req.params.name, "$options": "i" },user_id: req.params.user_id},
            {productDetailArr: 0});
        res.json(product)
    } catch (error) {
        res.status  (500).json({message: error.message})
    }  
})
//Getting producer's products
router.get('/:id/products/:page/', verifyToken, async (req, res) => {
    try{
        const page = parseInt(req.params.page)
        const limit = 10//req.params.limit
        let skip = (page-1) * limit
        let totalDocs = 0;
        
        let producer = await Producer.findById(req.params.id)
        totalDocs = producer.productArr.length
        
        await Producer.findById(req.params.id)
        .populate({path:'productArr', select: '-productDetailArr', skip: skip, limit: limit})
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            // console.log(element)
            let productArr = {
                docs: [],
                totalDocs: totalDocs,
                limit: limit,
                totalPages: Math.ceil(totalDocs/limit),
                page: page
            }
            productArr.docs = element.productArr
            res.json(productArr);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

// function getTotal(items){
//     try {
//         let docs = []
//         for(let i = 0; i < items.length; i++){
//             Product_producer.findById(items[i]._id)
//             .populate({
//                 path: 'productDetailArr',
//                 match: { amountLeft: { $gt: 0 } },
//             })
//             .exec(async function(error, element){
//                 if (error) return res.status(400).json({message: error.message})
                
//                 let total_amount_remained = 0;
//                 for(let i = 0; i < element.productDetailArr.length; i++){
//                     let amountLeft = parseInt(element.productDetailArr[i].amountLeft);
//                     total_amount_remained = total_amount_remained + amountLeft
//                 }
//                 docs.push(total_amount_remained) 
//             })
//         }
//         // console.log(docs)
//         return docs
//     } catch (error) {
//         return error
//     }
// }

//Getting all producer's products
router.get('/products/all/:page/:user_id', verifyToken, async (req, res) => {
    try{
        var query = {user_id: req.params.user_id};
        var options = {
            page: req.params.page,
            limit: 9,
            select: 'name measure'
        };
        // let docs2 = [0,0,0,0,0,0,0,0,0,0];
        // let total_amount_remained = 0;
        Product_producer.paginate(query, options).then(async function (result) {
            // for await (const doc of Product_producer.find({_id: {$in: result.docs}}).select('productDetailArr').populate({path: 'productDetailArr', match: { amountLeft: { $gt: 0 } },})) {
                
            //     for await(let element of doc.productDetailArr){
            //         let amountLeft = parseInt(element.amountLeft);
            //         total_amount_remained = total_amount_remained + amountLeft
            //     }
            //     for await (let elem of docs2){
            //         elem.total_amount_remained = total_amount_remained;
            //     }
            // }
            res.json(result);
            
        }).catch(function(error) {
            console.log(error); 
        })
        
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

//Getting producer product's detail
router.get('/:id/product/detail/:page', verifyToken, async (req, res) => {
    try{
        const page = parseInt(req.params.page)
        const limit = 10//req.params.limit
        let skip = (page-1) * limit
        let totalDocs = 0;
        
        let product = await Product_producer.findById(req.params.id)
        totalDocs = product.productDetailArr.length


        await Product_producer.findById(req.params.id)
        .populate({
            path: 'productDetailArr',
            match: { amountLeft: { $gt: 0 } },
            options: {
                sort:{ },
                skip: skip,
                limit : limit
            },
          })
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            
            let productDetailArr = {
                docs: [],
                totalDocs: totalDocs,
                limit: limit,
                totalPages: Math.ceil(totalDocs/limit),
                page: page
            }
            productDetailArr.docs = element.productDetailArr
            // console.log(productDetailArr);
            res.json(productDetailArr);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

//Getting producer products detail total remained calculation
router.get('/:id/product/detail/calculate/total', verifyToken, async (req, res) => {
    try{
        await Product_producer.findById(req.params.id)
        .populate({
            path: 'productDetailArr',
            match: { amountLeft: { $gt: 0 } },
          })
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            
            let total_amount_remained = 0;
            for(let i = 0; i < element.productDetailArr.length; i++){
              let amountLeft = parseInt(element.productDetailArr[i].amountLeft);
              total_amount_remained = total_amount_remained + amountLeft
            }
            res.json(total_amount_remained);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

//Getting producer products detail total payed calculation
router.get('/:id/product/detail/calculate/payed', verifyToken, async (req, res) => {
    try{
        await Product_producer.findById(req.params.id)
        .populate({
            path: 'productDetailArr',
            match: { amountLeft: { $gt: 0 } },
          })
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            
            let totalPayed = 0;
            for(let i = 0; i < element.productDetailArr.length; i++){
                let payed = parseInt(element.productDetailArr[i].payed.replace(/[^0-9]/g,''));
                totalPayed = totalPayed + payed
            }
            totalPayed = totalPayed.toString()
            totalPayed = totalPayed.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            res.json(totalPayed);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

//Getting producer products detail total unpayed calculation
router.get('/:id/product/detail/calculate/unpayed', verifyToken, async (req, res) => {
    try{
        await Product_producer.findById(req.params.id)
        .populate({
            path: 'productDetailArr',
            match: { amountLeft: { $gt: 0 } },
          })
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            
            let totalUnPayed = 0;
            for(let i = 0; i < element.productDetailArr.length; i++){
                let unPayed = parseInt(element.productDetailArr[i].unPayed.replace(/[^0-9]/g,''));
                totalUnPayed = totalUnPayed + unPayed
            }
            totalUnPayed = totalUnPayed.toString()
            totalUnPayed = totalUnPayed.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            res.json(totalUnPayed);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

//Getting producer product's detail history
router.get('/:id/product/detail/history/:page', verifyToken, async (req, res) => {
    try{
        let get_page = (req.params.page * 10) - 10
        await Product_producer.findById(req.params.id)
        .populate({
            path: 'productDetailArr',
            match: { amountLeft: { $eq: 0 } },
            options: {
                sort: {},
                skip: get_page,
                limit: 10
            }
          })
        .exec(async function(error, element){
            if (error) return res.status(400).json({message: error.message})
            
            // console.log(element.productDetailArr);
            res.json(element.productDetailArr);
        })
    }catch(error){
        res.status(500).json({message: error.message})
    }
})

// //Getting one
// router.get('/:id', verifyToken, (req, res) => {
//     res.json(res.producer)
// })

//Creating one
router.post('/', verifyToken, async (req, res) => {
    try {
        const producer = new Producer({
            user_id: req.body.user_id,
            name: req.body.name,
            phone: req.body.phone,
            companyName: req.body.companyName,
        })

        await producer.save()
        res.status(201).json('Producer successfully added!')
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
// Add product to producer
router.post('/:id/add/product/', verifyToken, async (req, res) => {
    try {
        let productNew = req.body.productArr
        productNew.user_id = req.body.user_id

        const product = new Product_producer(productNew)

        await Producer.findById(req.params.id)
        .populate('productArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})

            await product.save();
            element.productArr.push(product);
    
            await element.save()
            res.json('Product successfully added!')
        });
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
//Add product detail to producer's product
router.post('/product/:pid/add/detail/', verifyToken, async (req, res) => {
    try {
        const product_detail = new Product_producer_detail(req.body.productDetailArr)

        await Product_producer.findById(req.params.pid)
        .populate('productDetailArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})
            console.log(element);
            await product_detail.save();
            element.productDetailArr.push(product_detail);
    
            await element.save()
            res.json('Product detail successfully added!')
        });
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
//Update one
router.patch('/:id', verifyToken, async (req, res) => {
    try {
        let producer = await Producer.findById(req.params.id);
        
        if(req.body.name != null){
            producer.name = req.body.name
        }
        if(req.body.phone != null){
            producer.phone = req.body.phone
        }
        if(req.body.companyName != null){
            producer.companyName = req.body.companyName
        }

        await producer.save()
        res.json('Producer successfully updated!')
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Update one product of producer
router.patch('/:id/product/:pid', verifyToken, async (req, res) => {
    try {
        let product = await Product_producer.findById(req.params.pid);
        switch (true) { 
            case req.body.barcode != null:
                product.barcode = req.body.barcode
            case req.body.name != null:
                product.name = req.body.name
            case req.body.amount != null:
                product.amount = req.body.amount
            case req.body.measure != null:
                product.measure = req.body.measure
            // case req.body.buyPrice != null:
            //     product.buyPrice = req.body.buyPrice
            case req.body.sellPrice != null:
                product.sellPrice = req.body.sellPrice
            case req.body.description != null:
                product.description = req.body.description
            // default:
                // res.status(400).json({message: 'error in switch'})
        }

        await product.save()
        res.json('Producer product successfully updated!')
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})
//Update one detail of product of producer
router.patch('/product/detail/:did', verifyToken, async (req, res) => {
    try {
        let product = await Product_producer_detail.findById(req.params.did);
        switch (true) { 
            case req.body.date != null:
                product.date = req.body.date
            case req.body.amount != null:
                product.amount = req.body.amount
            case req.body.amountLeft != null:
                product.amountLeft = req.body.amountLeft
            case req.body.buyPrice != null:
                product.buyPrice = req.body.buyPrice
            case req.body.sellPrice != null:
                product.sellPrice = req.body.sellPrice
            case req.body.payed != null:
                product.payed = req.body.payed
            case req.body.unPayed != null:
                product.unPayed = req.body.unPayed
            case req.body.about != null:
                product.about = req.body.about
            // default:
                // res.status(400).json({message: 'error in switch'})
        }

        await product.save()
        res.json('Producer product deatil successfully updated!')
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})

//Delete one
router.delete('/:id', verifyToken, async (req, res) => {
    try {
        let producer = await Producer.findById(req.params.id);
        
        for await (const doc of Product_producer.find({_id: {$in: producer.productArr}})) {
            await Product_producer_detail.deleteMany({_id: {$in: doc.productDetailArr}});
            await doc.remove();
        }
       
        await producer.remove()
        res.json({message: 'Producer deleted!', deleted: true})
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Delete one product of producer
router.delete('/:id/product/:pid', verifyToken, async (req, res) => {
    try {
        let product = await Product_producer.findById(req.params.pid)
        await Product_producer_detail.deleteMany({_id: {$in: product.productDetailArr}});

        await Producer.findById(req.params.id)
        .populate('productArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})

            let arrayIndex = await element.productArr.findIndex(function( currentValue ) {
                return currentValue._id == req.params.pid; 
            })
            await product.remove();
            
            await element.productArr.splice(arrayIndex, 1);
            await element.save()
            res.json({message: 'Product of producer deleted!', deleted: true})
        });
        
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Delete one detail of one product of producer
router.delete('/product/:pid/detail/:did', verifyToken, async (req, res) => {
    try {
        let product_detail = await Product_producer_detail.findById(req.params.did)

        await Product_producer.findById(req.params.pid)
        .populate('productDetailArr')
        .exec(async function (error, element) {
            if (error) return res.status(400).json({message: error.message})

            let arrayIndex = await element.productDetailArr.findIndex(function( currentValue ) {
                return currentValue._id == req.params.did; 
            })
            await product_detail.remove();
            
            await element.productDetailArr.splice(arrayIndex, 1);
            await element.save()
            res.json({message: 'Detail of one product of producer deleted!', deleted: true})
        });
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})







//Verify token
function verifyToken(req, res, next){
    const bearerHeader = req.headers['authorization'];

    if(typeof bearerHeader !== 'undefined'){
        jwt.verify(bearerHeader, 'personlovescats', (err, auth) => {
            if (err){
                res.sendStatus(403);
            }else{
               next()
            }
        })
        // const bearer = bearerHeader.split(' ');
        // const bearerToken = bearer[1];
        // req.token =  bearerToken;
    }else{  
        //Forbidden
        res.status(403);
    }
}

module.exports = router
