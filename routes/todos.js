const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

const Todo = require('../models/todos-list')

//Getting all todos
router.get('/get/all/:user_id', verifyToken, async (req, res) => {
    try{
        const todos = await Todo.find({user_id: req.params.user_id});
        res.json(todos);
    }catch(error){
        res.status(500).json({message: error.message});
    }
})


// Add todo
router.post('/add/todos', verifyToken, async (req, res) => {
    try {
        let resp = await Todo.findOne({user_id: req.body.user_id})

        if(resp !== null){
            if(req.body.length !== 0){
                resp.todos = req.body.todos
            }
            // console.log(resp)
            await resp.save();
            res.status(201).json('Todos successfully modified!')
        }else{
            let todo = { todos: []}
            todo.todos = req.body.todos
            todo.user_id = req.body.user_id
            // console.log(todo);
            let newTodo = await new Todo(todo)
            await newTodo.save();
            res.status(201).json('Todos successfully added!')
        }
        
    } catch (error) {
        res.status(400).json({message: error.message})
    }
    
})


// router.get('/version/check', verifyToken, async (req, res) => {
//     try {
//         let latestVersion = await LatestVersion.find()

//         if(latestVersion == req.body.version){
//             res.status(201).json({version: latestVersion, updateRequired: false})
//         }else{
//             res.status(201).json({version: latestVersion, updateRequired: true})
//         }
        
//     } catch (error) {
//         res.status(400).json({message: error.message})
//     }
// })



//Verify token
function verifyToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){
        jwt.verify(bearerHeader, 'personlovescats', (err, auth) => {
            if (err){
                res.sendStatus(403);
            }else{
               next()
            }
        })
    }else{  
        //Forbidden
        res.status(403);
    }
}

module.exports = router